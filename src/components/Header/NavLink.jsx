import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

function NavLink(props) {

  let isActive = useLocation().pathname === props.to;
  let className = isActive ? 'nav-item nav-link active' : 'nav-item nav-link';

  return (
    <Link className={className} {...props}> {props.children}</Link>
  );
}

NavLink.contextTypes = {
  router: PropTypes.object
};

export default NavLink;