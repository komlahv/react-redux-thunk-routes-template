import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import Header from '../Header';

const renderer = new ShallowRenderer();

test('renders without crashing', () => {
  const div = document.createElement('div');
  renderer.render(<Header />, div);
  document.children[0].remove();
});
