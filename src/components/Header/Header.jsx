import React from 'react'
import { Link } from "react-router-dom";
import NavLink from './NavLink';

function Header(props) {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link className="navbar-brand" to="/">Navbar</Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <NavLink to="/">Home</NavLink>
          <NavLink to="/cash">Cash</NavLink>
          <NavLink to="/form">Form</NavLink>
          <NavLink to="/contacts">Contacts</NavLink>
          <NavLink to="/crud">Crud</NavLink>
        </div>
      </div>
    </nav>
  )
}

export default Header
