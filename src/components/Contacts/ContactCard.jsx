import React from "react"
import styled from 'styled-components';

function ContactCard(props) {
  console.log(props)

  const Card = styled.div`
    width: 280px;
    background: #ffffff;
    margin: 5px auto;
    box-sizing: border-box;
    border-radius: 16px;
    box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.19);
    -webkit-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.19);
    -moz-box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.19);
  `;

  const Img = styled.img`
    width: 100%;
    height:200px;
  `;

  return (
    <Card className="contact-card">
      <Img src={props.contact.imgUrl} alt="Kitty" />
      <h3>{props.contact.name}</h3>
      <p>Phone: {props.contact.phone}</p>
      <p>Email: {props.contact.email}</p>
    </Card>
  )
}

export default ContactCard