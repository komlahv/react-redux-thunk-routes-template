import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components';

const Foot = styled.div`
  background-color: yellow;
  height: fit-content;
  padding: 8px 0;
  line-height:50px;
`;

function Footer(props) {
  return (
    <Foot>
      <p>My little styled-component footer to show <b>{props.name}</b></p>
    </Foot>
  )
}

Footer.propTypes = {
  name: PropTypes.string
}

Footer.defaultProps = {
  name: 'my-default-prop'
}

export default Footer
