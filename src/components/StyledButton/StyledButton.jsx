import styled from 'styled-components';

const StyledButton = styled.button`
  font: inherit;
  cursor: pointer;
  border: 1px solid blue;
  background: blue;
  color: white;
  padding: 0.5rem 2rem;
  margin: 1rem;

  :focus, :hover {
    outline: none;
  }

  :active {
    background: lightblue;
    color: blue;
  }
`;

export default StyledButton