// export const cashUp = val => {
//   return { type: "CASH_UP", value: val };
// };

export const cashDown = val => {
  return { type: "CASH_DOWN", value: val };
};

//redux-thunk handles the async operations using dispatch, simply follow this pattern
export const cashUp = val => {
  return async dispatch => {
    let res = await (await fetch('https://jsonplaceholder.typicode.com/posts/3')).json();
    console.log(res);
    dispatch({
      type: "CASH_UP",
      value: res.id
    })
  };
};