
//redux-thunk handles the async operations using dispatch, simply follow this pattern
export const updateUserInfo = event => {
  const { name, value, type, checked } = event.target;

  return {
    type: "UPDATE_USER_DATA",
    payload: {
      name,
      value,
      type,
      checked
    }
  };
};

const Done = () => {
  return {
    type: "DONE",
  };
};

export const submitInfo = (event) => {
  event.preventDefault();
  const { firstName, lastName, box, isFriendly, gender, favColor } = event.target.elements;
  let data = {
    firstName: firstName.value,
    lastName: lastName.value,
    box: box.value || 'no value',
    isFriendly: isFriendly.checked,
    gender: gender.value || 'not given',
    favColor: favColor.value
  };
  console.log(event.target.elements.firstName.value);

  return async dispatch => {
    // dispatch(loading());
    try {
      let res = await (await fetch('https://webhook.site/b1bfb022-2441-4dc6-a705-1ced874f723b', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify(data)
      })).json();
      console.log(res);
    } catch (e) { console.log(e); }
    dispatch(Done());
  };
};