export const loading = () => {
  return {
    type: "LOADING"
  };
};

//redux-thunk handles the async operations using dispatch, simply follow this pattern
export const ageUp = val => {
  return async dispatch => {
    dispatch(loading());
    let res = await (await fetch('https://jsonplaceholder.typicode.com/posts/3')).json();
    console.log(res);
    dispatch({
      type: "AGE_UP",
      value: res.id
    })
  };
};

export const ageDown = val => {
  return { type: "AGE_DOWN", value: val };
};