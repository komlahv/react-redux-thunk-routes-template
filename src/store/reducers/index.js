import { combineReducers } from 'redux';

import ageReducer from "./ageReducer";
import cashReducer from "./cashReducer";
import formReducer from "./formReducer";

// 'ageReducer: ageReducer' can be reduce to just 'ageReducer'
const rootReducer = combineReducers({
  ageReducer,
  cashReducer,
  formReducer
})

export default rootReducer;