const initialState = {
  cash: 50,
};

const reducer = (state = initialState, action) => {
  const newState = { ...state };

  switch (action.type) {
    case "CASH_UP":
      newState.cash += action.value;
      newState.loading = false;
      break;

    case "CASH_DOWN":
      newState.cash -= action.value;
      break;
    default:
  }
  return newState;
};

export default reducer;
