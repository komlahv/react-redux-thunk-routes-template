const initialState = {
  firstName: "",
  lastName: "",
  box: "",
  isFriendly: true,
  gender: "",
  favColor: "blue"
};

const reducer = (state = initialState, action) => {
  let newState = { ...state };

  switch (action.type) {
    case "UPDATE_USER_DATA":
      action.payload.type === "checkbox" ?
        newState = {
          ...newState,
          [action.payload.name]: action.payload.checked,
        } :
        newState = {
          ...newState,
          [action.payload.name]: action.payload.value,
        };
      break;

    default:
  }
  return newState;
};

export default reducer;
