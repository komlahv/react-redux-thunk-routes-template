import React from 'react'
import PropTypes from 'prop-types'
import './CashCounter.css'
import StyledButton from '../../components/StyledButton/StyledButton';

import { connect } from "react-redux";
import * as actionCreator from "../../store/actions/cashActions";

function CashCounter(props) {
  return (
    <section className="cash-block">
      <div className="cash-label">
        your Cash: <span>{props.cash}</span>
      </div>
      <StyledButton onClick={() => props.onCashUp(1)}>Cash UP</StyledButton>
      <StyledButton onClick={() => props.onCashDown(1)}>Cash Down</StyledButton>
    </section>
  )
}

CashCounter.propTypes = {
  cash: PropTypes.number.isRequired,
}

const mapStateToProps = state => {
  return {
    cash: state.cashReducer.cash,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCashUp: payload => dispatch(actionCreator.cashUp(payload)),
    onCashDown: payload => dispatch(actionCreator.cashDown(payload))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CashCounter);
