import React from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components';

import { connect } from "react-redux";
import * as actionCreator from "../../store/actions/formActions";

const MyForm = styled.form`
  padding: 12px 0;
  border: 5px solid red;
`;

const TextInput = styled.input`
  margin: 12px 0;
  padding: 8px 0;
  width: 50%;
`;
function Form(props) {
  //set value equal to state to make a controlled form and ensure state is the single source of truth
  //make input name attr/prop match the state name to handle change
  return (
    <MyForm onSubmit={(event) => props.handleSubmit(event)}>
      <TextInput type="text" value={props.firstName} name="firstName" placeholder="First Name" onChange={(event) => props.updateInfo(event)} />
      <br />

      <TextInput type="text" value={props.lastName} name="lastName" placeholder="Last Name" onChange={(event) => props.updateInfo(event)} />
      <br />

      <textarea name="box" placeholder="Enter text" value={props.box} onChange={(event) => props.updateInfo(event)} />
      <br />

      <label>
        <input type="checkbox" name="isFriendly" checked={props.isFriendly} onChange={(event) => props.updateInfo(event)} /> Is friendly?
        </label>
      <br />

      <label><input type="radio" name="gender" value="male" checked={props.gender === "male"} onChange={(event) => props.updateInfo(event)} />
          Male
        </label>
      <br />
      <label><input type="radio" name="gender" value="female" checked={props.gender === "female"} onChange={(event) => props.updateInfo(event)} />
          Female
         </label>
      <br />

      <label>Favorite Color:</label>
      <select value={props.favColor} onChange={(event) => props.updateInfo(event)} name="favColor">
        <option value="blue">Blue</option>
        <option value="green">Green</option>
        <option value="red">Red</option>
        <option value="orange">Orange</option>
        <option value="yellow">Yellow</option>
      </select>

      <h1>{props.firstName} {props.lastName}</h1>
      <h2>You are {props.gender}</h2>
      <h2>Your favorite color is {props.favColor}</h2>

      <button>Submit</button>
      {/* Formik to make easy react forms */}
    </MyForm>
  )
}

Form.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  box: PropTypes.string,
  isFriendly: PropTypes.bool,
  gender: PropTypes.string,
  favColor: PropTypes.string,
}

const mapStateToProps = state => {
  return {
    firstName: state.formReducer.firstName,
    lastName: state.formReducer.lastName,
    box: state.formReducer.box,
    isFriendly: state.formReducer.isFriendly,
    gender: state.formReducer.gender,
    favColor: state.formReducer.favColor,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateInfo: payload => dispatch(actionCreator.updateUserInfo(payload)),
    handleSubmit: payload => dispatch(actionCreator.submitInfo(payload)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);