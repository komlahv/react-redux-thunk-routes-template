import React from 'react'
import PropTypes from 'prop-types'
import './AgeCounter.css'
import StyledButton from '../../components/StyledButton/StyledButton';

import { connect } from "react-redux";
import * as actionCreator from "../../store/actions/ageActions";

import logo from "../../logo.svg";

function AgeCounter(props) {
  return (
    <section className="age-block">
      <div className="age-label">
        your age: <span>{props.age}</span> <br />
      </div>
      <StyledButton onClick={() => props.onAgeUp(1)}>Age UP</StyledButton>
      <StyledButton onClick={() => props.onAgeDown(1)}>Age Down</StyledButton>
      {props.loading && <img src={logo} className="App-logo" alt="logo" />}
    </section>
  )
}

AgeCounter.propTypes = {
  age: PropTypes.number.isRequired,
}

const mapStateToProps = state => {
  return {
    age: state.ageReducer.age,
    loading: state.ageReducer.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAgeUp: payload => dispatch(actionCreator.ageUp(payload)),
    onAgeDown: payload => dispatch(actionCreator.ageDown(payload)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AgeCounter);
