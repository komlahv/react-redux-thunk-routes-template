import React from 'react';
import useTableSorter from './sorter';
import styled from 'styled-components';

const TableHeader = styled.div`
  background-color: none;
`;

const TextInput = styled.input`
  width: 50%;
`;

const SmallTextInput = styled(TextInput)`
  width: 40%;
`;

let itemBeingEdited = {};

function handleChange(event) {
  //pull these attr from the element
  const { name, value } = event.target;
  itemBeingEdited = {
    ...itemBeingEdited,
    [name]: value
  }
}

const ProductTable = (props) => {
  const { items, requestSort } = useTableSorter(props.products);

  return (
    <table className="table table-hover">
      <thead>
        <tr>
          <th>
            <TableHeader
              type="button"
              onClick={() => requestSort('name')}
            >
              Name
            </TableHeader>
          </th>
          <th>
            <TableHeader
              type="button"
              onClick={() => requestSort('price')}
            >
              Price
            </TableHeader>
          </th>
          <th>
            <TableHeader
              type="button"
              onClick={() => requestSort('stock')}
            >
              In Stock
            </TableHeader>
          </th>
        </tr>
      </thead>
      <tbody>
        {items.map((item) => {
          if (props.editing === item.id) {
            return (
              <tr key={item.id}>
                <td><TextInput type="text" defaultValue={item.name} name="name" placeholder="Name" onChange={handleChange} /></td>
                <td><SmallTextInput type="number" defaultValue={item.price} name="price" placeholder="Price" onChange={handleChange} /></td>
                <td><SmallTextInput type="number" defaultValue={item.stock} name="stock" placeholder="Stock" onChange={handleChange} /></td>
                <td>
                  <button onClick={() => props.submitEdit(item.id, itemBeingEdited)}>Save</button>
                  {` - `}
                  <button onClick={props.cancelEdit}>Cancel</button>
                </td>
              </tr>
            )
          }
          return (
            <tr key={item.id}>
              <td>{item.name}</td>
              <td>${item.price}</td>
              <td>{item.stock}</td>
              <td>
                <button onClick={() => props.editProduct(item.id)}>Edit</button>
                {` - `}
                <button onClick={() => props.deleteProduct(item.id)}>Delete</button>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  );
};

export default ProductTable;