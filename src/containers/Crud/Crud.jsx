import React, { Component } from 'react';
import ProductTable from './ProductTable';
import ProductInput from './ProductInput';
class Crud extends Component {

  state = {
    editing: false,
    products: [
      { id: 1, name: 'Cheese', price: 4.9, stock: 20 },
      { id: 2, name: 'Milk', price: 1.9, stock: 32 },
      { id: 3, name: 'Yoghurt', price: 2.4, stock: 12 },
      { id: 4, name: 'Heavy Cream', price: 3.9, stock: 9 },
      { id: 5, name: 'Butter', price: 0.9, stock: 99 },
      { id: 6, name: 'Sour Cream ', price: 2.9, stock: 86 },
      { id: 7, name: 'Fancy French Cheese 🇫🇷', price: 99, stock: 12 },
      { id: 8, name: 'Burger cheese', price: 40, stock: 13 },
    ]
  };

  addProduct(item) {
    let id = this.state.products.length + 1;
    let parsedData = {
      ...item,
      id,
      name: `${item.name.charAt(0).toUpperCase()}${item.name.substring(1)}`
    }
    let updatedList = [...this.state.products, parsedData];
    console.log(updatedList);
    this.setState({
      products: updatedList
    })
  }

  editProduct(id) {
    this.setState({ editing: id });
  }

  cancelEdit() {
    this.setState({ editing: false });
  }

  submitEdit(id, data) {
    console.log(id, data);
    let items = this.state.products;

    let updatedList = items.map(item => {
      if (item.id === id) {
        item = {
          ...item,
          ...data
        }
      }
      return item;
    })

    this.setState({ editing: false, products: updatedList });
  }

  deleteProduct(id) {
    let filteredList = this.state.products.filter((el) => el.id !== id);
    this.setState({
      products: filteredList
    })
  }

  render() {
    return (
      <div>
        <ProductInput addProduct={(item) => this.addProduct(item)} />
        <ProductTable
          editing={this.state.editing}
          products={this.state.products}
          editProduct={(id) => this.editProduct(id)}
          submitEdit={(id, data) => this.submitEdit(id, data)}
          cancelEdit={() => this.cancelEdit()}
          deleteProduct={(id) => this.deleteProduct(id)}
        />
      </div>
    )
  }
}

export default Crud;
