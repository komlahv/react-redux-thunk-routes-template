import React, { Component } from 'react'
import styled from 'styled-components'

const TextInput = styled.input`
  margin: 24px 4px;
  padding: 8px 0;
  width: 24%;
`;

const SmallTextInput = styled(TextInput)`
  width: 12%;
`;

class ProductInput extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      price: 0,
      stock: 0
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    //pull these attr from the element
    const { name, value } = event.target;
    name === 'name' ? this.setState({ [name]: value }) : this.setState({ [name]: Number(value) });
  }

  render() {
    return (
      <div>
        <TextInput type="text" value={this.state.firstName} name="name" placeholder="Name" onChange={this.handleChange} />
        <SmallTextInput type="number" value={this.state.firstName} name="price" placeholder="Price" onChange={this.handleChange} />
        <SmallTextInput type="number" value={this.state.firstName} name="stock" placeholder="Stock" onChange={this.handleChange} />
        <button onClick={() => this.props.addProduct(this.state)}>Add</button>
      </div>
    )
  }
}

export default ProductInput
