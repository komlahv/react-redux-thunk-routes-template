import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Contacts from './components/Contacts/Contacts';

import AgeCounter from './containers/AgeCounter/AgeCounter';
import CashCounter from './containers/CashCounter/CashCounter';
import Form from './containers/Form/Form';
import Crud from './containers/Crud/Crud';

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <Route exact path='/' component={AgeCounter} />
        <Route path='/cash' component={CashCounter} />
        <Route path='/form' component={Form} />
        <Route path='/contacts' component={Contacts} />
        <Route path='/crud' component={Crud} />
        <Footer />
      </div>
    </Router>
  );
}

export default App;
