import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import App from './App';

const renderer = new ShallowRenderer();

test('renders without crashing', () => {
  const div = document.createElement('div');
  renderer.render(<App />, div);
  document.children[0].remove();
});
